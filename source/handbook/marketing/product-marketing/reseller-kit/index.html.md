---
layout: markdown_page
title: "Reseller Marketing Kit"
---

## Getting Started

The information contained in this section will provide you with solid understanding of who is GitLab, what do we do and where are we going in the future.

* [GitLab narrative and pitch](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit#slide=id.g39d65c7ce1_12_233)
* [GitLab elevator pitch](/handbook/marketing/product-marketing/#elevator-pitch)
* [GitLab solutions](/solutions/)
* [GitLab pricing](/pricing/)
* [Value of selling GitLab Ultimate](/pricing/ultimate/)
* [GitLab product vision](/direction/product-vision/)
* [GitLab personas](/handbook/marketing/product-marketing/roles-personas/)
* [GitLab compared to other tools](/devops-tools/)

Join GitLab’s Slack Reseller channel by emailing vgoetz@gitlab.com to keep current on new content assets, upcoming sales and technical training sessions and more.

## Training & Enablement

* [GitLab Reseller training webcast recordings](/webcast/reseller/)
* [GitLab Product training](/training/) - Providing a subset of on-demand content suggested based on role and skill level. Each track links to the specific on-demand training area GitLab recommends.
* [GitLab sales enablement](https://www.youtube.com/playlist?list=PLFGfElNsQthYe-_LZdge1SVc1XEM1bQfG)
* [Future GitLab sales enablement](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement)

## Marketing Resources

* [GitLab boilerplate web content](https://docs.google.com/document/d/1jzC2l88sKPDUWSXEgae4tqAg_QR34RDl6mPN5V8a0Mw/edit?usp=sharing)
* [GitLab official logo, guidelines and other artwork](/press/) - scroll all the way down to the *Images & Logos* section
* Reseller marketing issue board (coming soon)

## Frequently Asked Questions

1. Can one organization implement Core, Starter, Premium and Ultimate at the same time?
1. What is the difference in GitLab Ultimate and GitLab Gold?
